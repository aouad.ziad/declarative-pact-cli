package za.showcase.pact.cli;

import za.showcase.pact.cli.feign.BrokerFeignClient;
import za.showcase.pact.cli.operations.EnvironmentOperation;
import za.showcase.pact.cli.specs.EnvironmentSpec;
import za.showcase.pact.cli.specs.ParticipantSpec;
import za.showcase.pactflow.client.api.model.EmbeddedDeployedVersion;
import za.showcase.pactflow.client.api.model.EmbeddedReleasedVersion;
import za.showcase.pactflow.client.api.model.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Stream;

@Component
public class BrokerClient {

  private final BrokerFeignClient feignClient;

  public BrokerClient(BrokerFeignClient feignClient) {
    this.feignClient = feignClient;
  }

  public Optional<EnvironmentSpec> describe(String environment) {
    return getEnvironments()
        .stream()
        .filter(one -> one.name().equals(environment))
        .findFirst();
  }

  public List<EnvironmentSpec> getEnvironments() {
    var envs = feignClient.getEnvironments();
    return envs.getEmbedded().getEnvironments()
        .stream()
        .map(this::toSpec)
        .toList();
  }

  public ApplySpecResult applySpec(EnvironmentSpec expectedSpec, boolean preview) {
    assertAllParticipantsExist(expectedSpec);

    var envName = expectedSpec.name();
    var optional = describe(envName);

    var emptyEnvironment = new EnvironmentSpec(envName);
    var initialState = optional.orElse(emptyEnvironment);

    List<EnvironmentOperation> createEnv = optional.isEmpty() ? List.of(EnvironmentOperation.createEnvironment(envName)) : List.of();

    var recordRelease = expectedSpec.released().stream()
        .filter(initialState::isReleasedAbsent)
        .map(p -> EnvironmentOperation.recordRelease(envName, p))
        .toList();

    var recordSupportEnded = initialState.released().stream()
        .filter(expectedSpec::isReleasedAbsent)
        .map(p -> EnvironmentOperation.recordSupportEnded(envName, p))
        .toList();

    var recordDeployment = expectedSpec.deployed().stream()
        .filter(initialState::isDeployedAbsent)
        .map(p -> EnvironmentOperation.recordDeployment(envName, p))
        .toList();

    var recordUndeployment = initialState.deployed().stream()
        .filter(expectedSpec::isDeployedAbsent)
        .map(p -> EnvironmentOperation.recordUndeployment(envName, p))
        .toList();

    var proceed = new AtomicBoolean(!preview);
    var operationResults = Stream.of(createEnv, recordRelease, recordSupportEnded, recordDeployment, recordUndeployment)
        .flatMap(List::stream)
        .map(operation -> operation.execute(feignClient, proceed))
        .toList();

    boolean success = operationResults.stream().noneMatch(o -> o.status() == OperationResult.OperationStatus.FAILED);
    var status = preview ?
        ApplySpecResult.ApplySpecStatus.PREVIEW : success ?
        ApplySpecResult.ApplySpecStatus.SUCCESS : ApplySpecResult.ApplySpecStatus.FAILED;

    var finalState = describe(envName).orElse(emptyEnvironment);

    return new ApplySpecResult(status, initialState, finalState, operationResults);
  }

  private void assertAllParticipantsExist(EnvironmentSpec expectedSpec) {
    Stream.of(expectedSpec.released(), expectedSpec.deployed()).flatMap(List::stream).forEach(p -> {
      var response = feignClient.getPacticipantVersion(p.name(), p.version());
      if (HttpStatus.NOT_FOUND.equals(response.getStatusCode())) {
        throw new ParticipantNotFoundException(p);
      }
    });
  }

  private EnvironmentSpec toSpec(Environment one) {
    var released = feignClient.getReleasedVersions(one.getUuid());
    var deployed = feignClient.getDeployedVersions(one.getUuid());

    var releasedSpecs = released.getEmbedded().getReleasedVersions()
        .stream()
        .map(this::toSpec)
        .toList();

    var deployedSpecs = deployed.getEmbedded().getDeployedVersions()
        .stream()
        .map(this::toSpec)
        .toList();

    return new EnvironmentSpec(one.getName(), releasedSpecs, deployedSpecs);
  }

  private ParticipantSpec toSpec(EmbeddedReleasedVersion one) {
    String name = one.getEmbedded().getPacticipant().getName();
    String version = one.getEmbedded().getVersion().getNumber();

    return new ParticipantSpec(name, version);
  }

  private ParticipantSpec toSpec(EmbeddedDeployedVersion one) {
    String name = one.getEmbedded().getPacticipant().getName();
    String version = one.getEmbedded().getVersion().getNumber();

    return new ParticipantSpec(name, version);
  }
}
