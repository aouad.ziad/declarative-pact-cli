package za.showcase.pact.cli.specs;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

public record EnvironmentSpec(
    String name,
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    List<ParticipantSpec> released,
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    List<ParticipantSpec> deployed
) {

  public EnvironmentSpec(String name) {
    this(name, List.of(), List.of());
  }

  @Override
  public List<ParticipantSpec> released() {
    return released != null ? released : List.of();
  }

  @Override
  public List<ParticipantSpec> deployed() {
    return deployed != null ? deployed : List.of();
  }

  public boolean isReleasedAbsent(ParticipantSpec p) {
    return !released().contains(p);
  }

  public boolean isDeployedAbsent(ParticipantSpec p) {
    return !deployed().contains(p);
  }
}