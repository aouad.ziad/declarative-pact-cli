package za.showcase.pact.cli.specs;

/**
 * This is all we need to represent a participant to the outside world.
 */
public record ParticipantSpec(String name, String version) {

  @Override
  public String toString() {
    return name + "@" + version;
  }
}
