package za.showcase.pact.cli.feign;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import za.showcase.pactflow.client.api.model.CreateUpdateEnvironment;
import za.showcase.pactflow.client.api.model.DeployedVersion;
import za.showcase.pactflow.client.api.model.DeployedVersionsForEnvironment;
import za.showcase.pactflow.client.api.model.Environment;
import za.showcase.pactflow.client.api.model.Environments;
import za.showcase.pactflow.client.api.model.PacticipantVersion;
import za.showcase.pactflow.client.api.model.ReleasedVersion;
import za.showcase.pactflow.client.api.model.ReleasedVersionsForEnvironment;

import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@FeignClient(name = "broker-feign-client", url = "${broker.url}")
public interface BrokerFeignClient {

  @GetMapping("/environments")
  Environments getEnvironments();

  default Optional<Environment> getEnvironment(String name) {
    return getEnvironments().getEmbedded().getEnvironments()
        .stream()
        .filter(e -> Objects.equals(name, e.getName()))
        .findAny();
  }

  @GetMapping("/pacticipants/{pacticipantName}/versions/{version}")
  ResponseEntity<PacticipantVersion> getPacticipantVersion(@PathVariable("pacticipantName") String participant,
                                                           @PathVariable("version") String version);

  @GetMapping("/environments/{environment-id}/released-versions/currently-supported")
  ReleasedVersionsForEnvironment getReleasedVersions(@PathVariable("environment-id") UUID environmentId);

  @GetMapping("/environments/{environment-id}/deployed-versions/currently-deployed")
  DeployedVersionsForEnvironment getDeployedVersions(@PathVariable("environment-id") UUID environmentId);

  @PostMapping("/environments")
  ResponseEntity<Environment> createEnvironment(@RequestBody CreateUpdateEnvironment creation);

  @PostMapping("/pacticipants/{pacticipantName}/versions/{version}/deployed-versions/environment/{environment-id}")
  ResponseEntity<DeployedVersion> recordDeployment(
      @PathVariable("pacticipantName") String participant,
      @PathVariable("version") String version,
      @PathVariable("environment-id") UUID environmentId
  );

  @PatchMapping("/deployed-versions/{deployed-version-id}")
  ResponseEntity<DeployedVersion> recordUndeployment(@PathVariable("deployed-version-id") UUID id);

  @RequestMapping(
      method = RequestMethod.PATCH,
      value = "/released-versions/{released-version-id}"
  )
  ResponseEntity<ReleasedVersion> recordSupportEnded(@PathVariable("released-version-id") UUID id, @RequestBody CurrentlySupported supported);

  @PostMapping(
      value = "/pacticipants/{pacticipantName}/versions/{version}/released-versions/environment/{environment-id}"
  )
  ResponseEntity<ReleasedVersion> recordRelease(
      @PathVariable("pacticipantName") String participant,
      @PathVariable("version") String version,
      @PathVariable("environment-id") UUID environmentId
  );

  /**
   * TODO Create the PactContract schema
   * <p>
   * Note that a pacticipant is created automatically when a PACT is published
   *
   * @see `https://docs.pact.io/pact_broker/advanced_topics/api_docs/publish_pact`
   * @see `https://docs.pact.io/pact_broker/advanced_topics/pacticipant`
   */
  @PutMapping("/pacts/provider/{provider}/consumer/{consumer}/version/{version}")
  void publishPact(
      @PathVariable("provider") String providerName,
      @PathVariable("consumer") String consumerName,
      @PathVariable("provider") String consumerVersion,
      @RequestBody Void pact
  );

  @GetMapping("/pacts/provider/{provider}/consumer/{consumer}/version/{version}")
  ResponseEntity<String> getPactBetween(
      @PathVariable("provider") String providerName,
      @PathVariable("consumer") String consumerName,
      @PathVariable("provider") String consumerVersion
  );

  /**
   * Example
   * <pre>
   * https://zaouad.pactflow.io/pacts/provider/scheduler/consumer/consumer/pact-version/84a47044fb992f575e83b103fd8030243afc4a4d/verification-results
   * </pre>
   */
  @PostMapping("/pacts/provider/{provider}/consumer/{consumer}/pact-version/{pactVersion}/verification-results")
  ResponseEntity<String> publishVerificationResults(
      @PathVariable("provider") String providerName,
      @PathVariable("provider") String consumerName,
      @PathVariable("pactVersion") String pactVersion,
      @RequestBody VerificationResult result
  );

  record VerificationResult(boolean success, String providerApplicationVersion, String buildUrl) {
  }

  record CurrentlySupported(boolean currentlySupported) {

    public CurrentlySupported {
      if (currentlySupported)
        throw new IllegalStateException("currentlySupported must be set to 'false'");
    }

    public static CurrentlySupported not() {
      return new CurrentlySupported(false);
    }
  }
}