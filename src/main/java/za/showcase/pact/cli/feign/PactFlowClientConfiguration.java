package za.showcase.pact.cli.feign;

import feign.RequestInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;

@Configuration
public class PactFlowClientConfiguration {

  @Bean
  public RequestInterceptor basicAuthRequestInterceptor(BrokerCredentials broker) {
    return request -> {
      // Tried putting these directly in the spring annotations - It didn't work.
      var contentType = HttpMethod.PATCH.matches(request.method()) ? "application/merge-patch+json" : "application/json";
      request
          .header("Authorization", "Bearer " + broker.token())
          .header("Accept", "application/hal+json")
          .header("Content-Type", contentType);
    };
  }
}