package za.showcase.pact.cli.feign;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("broker")
public record BrokerCredentials(String url, String token, String username, String password) {

  boolean isTokenBased() {
    return token != null;
  }
}
