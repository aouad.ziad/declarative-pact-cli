package za.showcase.pact.cli;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator;
import za.showcase.pact.cli.specs.EnvironmentSpec;
import org.springframework.shell.command.CommandHandlingResult;
import org.springframework.shell.command.annotation.Command;
import org.springframework.shell.command.annotation.ExceptionResolver;
import org.springframework.shell.command.annotation.Option;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;

@Command
@Component
public class DeclarativePACTCommandLine {

  static final ObjectMapper YAML = new ObjectMapper(
      YAMLFactory.builder()
          .disable(YAMLGenerator.Feature.WRITE_DOC_START_MARKER)
          .build()
  );

  private final BrokerClient client;

  public DeclarativePACTCommandLine(BrokerClient client) {
    this.client = client;
  }

  @Command
  public String listEnvironments() throws JsonProcessingException, BaseCliException {
    var result = new ListEnvironmentResult(client.getEnvironments().stream().map(EnvironmentSpec::name).toList());
    return YAML.writerWithDefaultPrettyPrinter().writeValueAsString(result);
  }

  @Command
  public String describeEnvironment(@Option(shortNames = 'e', longNames = "env") String name) throws JsonProcessingException, BaseCliException {
    var env = client.describe(name);
    var v = env.orElseThrow(() -> new EnvironmentNotFoundException(name));

    return YAML.writerWithDefaultPrettyPrinter().writeValueAsString(v);
  }

  @Command
  public String applySpec(
      @Option(shortNames = 's', longNames = "spec", defaultValue = "spec.yaml") File specFile,
      @Option(shortNames = 'p', longNames = "preview", defaultValue = "false") boolean preview
  ) throws IOException, BaseCliException {
    if (!specFile.exists()) {
      throw new ResourceNotFoundException(specFile.toPath());
    }

    var spec = YAML.readValue(specFile, EnvironmentSpec.class);
    var result = client.applySpec(spec, preview);

    return YAML.writerWithDefaultPrettyPrinter().writeValueAsString(result);
  }

  @ExceptionResolver
  public CommandHandlingResult handle(BaseCliException e) throws JsonProcessingException {
    return CommandHandlingResult.of(YAML.writerWithDefaultPrettyPrinter().writeValueAsString(e.problem()));
  }
}
