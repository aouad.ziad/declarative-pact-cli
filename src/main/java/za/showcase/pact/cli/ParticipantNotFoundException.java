package za.showcase.pact.cli;

import za.showcase.pact.cli.specs.ParticipantSpec;

public class ParticipantNotFoundException extends BaseCliException {

  private final ParticipantSpec participant;

  public ParticipantNotFoundException(ParticipantSpec participant) {
    super(participant + " not found");
    this.participant = participant;
  }

  @Override
  public Problem problem() {
    return Problem.builder()
        .withCode("participant-not-found")
        .with("participant", participant.toString())
        .build();
  }
}