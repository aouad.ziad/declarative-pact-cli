package za.showcase.pact.cli;

import za.showcase.pact.cli.feign.BrokerCredentials;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.shell.command.annotation.CommandScan;
import org.springframework.shell.command.annotation.EnableCommand;

@SpringBootApplication
@EnableFeignClients
@EnableConfigurationProperties(BrokerCredentials.class)
@CommandScan
@EnableCommand(DeclarativePACTCommandLine.class)
public class DeclarativePactCliApplication {

  public static void main(String[] args) {
    SpringApplication.run(DeclarativePactCliApplication.class, args);
  }
}
