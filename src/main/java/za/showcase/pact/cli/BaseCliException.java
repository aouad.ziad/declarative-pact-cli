package za.showcase.pact.cli;

public abstract class BaseCliException extends RuntimeException {

  protected BaseCliException(String message) {
    super(message);
  }

  public abstract Problem problem();
}
