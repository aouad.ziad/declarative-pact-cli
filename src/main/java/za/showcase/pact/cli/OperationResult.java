package za.showcase.pact.cli;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonValue;
import jakarta.annotation.Nullable;
import za.showcase.pact.cli.specs.ParticipantSpec;

/**
 * Denotes a participant operation on an environment, so basically one of
 * <ul>
 *   <li>deploy</li>
 *   <li>un-deploy</li>
 *   <li>release</li>
 *   <li>un-release</li>
 * </ul>
 * <p>
 * When we diff and expected versus an actual environment we end up with a series of operations we have to do to make them match.
 * This is the result of such an operation.
 */
public record OperationResult(
    OperationType type,

    String environment,

    @Nullable
    @JsonInclude(JsonInclude.Include.NON_NULL)
    ParticipantSpec participant,
    OperationStatus status) {

  @Override
  public String toString() {
    var sb = new StringBuilder()
        .append("[").append(environment).append("] ");

    if (participant != null) {
      sb.append(participant).append(" ");
    }

    sb.append("- ").append(status.code());

    return sb.toString();
  }

  public enum OperationStatus {
    SUCCESS,

    FAILED,

    /**
     * Used to denote an operation was skipped because the previous operation failed and there is no need to continue, or because we are in preview mode.
     */
    SKIPPED;

    @JsonValue
    String code() {
      return name().toLowerCase();
    }
  }

  public enum OperationType {
    CREATE_ENVIRONMENT,
    RECORD_RELEASE,
    RECORD_SUPPORT_ENDED,
    RECORD_DEPLOYMENT,
    RECORD_UNDEPLOYMENT;

    @JsonValue
    public String code() {
      return name().toLowerCase().replace('_', '-');
    }
  }
}
