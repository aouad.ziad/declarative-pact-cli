package za.showcase.pact.cli;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Inspired form Zalando's spring-web-problems.
 */
public final class Problem {

  @JsonProperty
  private final String code;
  @JsonProperty
  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  private final String message;
  @JsonProperty
  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  private final Map<String, String> details;

  public static ProblemBuilder builder() {
    return new ProblemBuilder();
  }

  private Problem(String code, String message, Map<String, String> details) {
    this.code = Objects.requireNonNull(code);
    this.message = Objects.requireNonNull(message);
    this.details = Map.copyOf(Objects.requireNonNull(details));
  }

  public static final class ProblemBuilder {
    private String code;
    private String message;
    private Map<String, String> details;

    public ProblemBuilder() {
      this.details = new HashMap<>();
      this.message = "";
    }

    public ProblemBuilder withCode(String code) {
      this.code = code;
      return this;
    }

    public ProblemBuilder withMessage(String message) {
      this.message = message;
      return this;
    }

    public ProblemBuilder with(String key, String value) {
      details.put(key, value);
      return this;
    }

    public Problem build() {
      return new Problem(code, message, details);
    }
  }
}
