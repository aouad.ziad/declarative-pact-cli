package za.showcase.pact.cli;

import java.util.List;

public record ListEnvironmentResult(List<String> environments) {
}
