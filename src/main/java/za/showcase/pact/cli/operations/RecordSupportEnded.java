package za.showcase.pact.cli.operations;

import za.showcase.pact.cli.OperationResult;
import za.showcase.pact.cli.feign.BrokerFeignClient;
import za.showcase.pact.cli.specs.ParticipantSpec;
import za.showcase.pactflow.client.api.model.EmbeddedReleasedVersion;
import org.springframework.http.HttpStatusCode;

import java.util.Objects;
import java.util.UUID;

class RecordSupportEnded extends EnvironmentOperation {

  RecordSupportEnded(ParticipantSpec participant, String environment) {
    super(OperationResult.OperationType.RECORD_SUPPORT_ENDED, participant, environment);
  }

  @Override
  public HttpStatusCode apply(BrokerFeignClient client) {
    var envId = client.getEnvironment(environment).get().getUuid();
    var uuidOfReleasedParticipant = findReleasedParticipantUUID(client, participant, envId);
    var response = client.recordSupportEnded(uuidOfReleasedParticipant, BrokerFeignClient.CurrentlySupported.not());
    return response.getStatusCode();
  }

  private static UUID findReleasedParticipantUUID(BrokerFeignClient client, ParticipantSpec p, UUID envId) {
    return client.getReleasedVersions(envId).getEmbedded().getReleasedVersions().stream()
        .filter(r -> {
          return
              Objects.equals(r.getEmbedded().getPacticipant().getName(), p.name())
                  && Objects.equals(r.getEmbedded().getVersion().getNumber(), p.version());
        })
        .map(EmbeddedReleasedVersion::getUuid)
        .findFirst()
        .orElseThrow(() -> new IllegalStateException("Unable to find the UUID of released " + p));
  }
}
