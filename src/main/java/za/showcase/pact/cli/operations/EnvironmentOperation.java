package za.showcase.pact.cli.operations;

import za.showcase.pact.cli.OperationResult;
import za.showcase.pact.cli.OperationResult.OperationStatus;
import za.showcase.pact.cli.OperationResult.OperationType;
import za.showcase.pact.cli.feign.BrokerFeignClient;
import za.showcase.pact.cli.specs.ParticipantSpec;
import org.springframework.http.HttpStatusCode;

import java.util.concurrent.atomic.AtomicBoolean;

public abstract class EnvironmentOperation {

  private final OperationType type;
  protected final ParticipantSpec participant;
  protected final String environment;

  public static EnvironmentOperation createEnvironment(String environment) {
    return new CreateEnvironment(environment);
  }

  public static EnvironmentOperation recordRelease(String environment, ParticipantSpec participant) {
    return new RecordRelease(participant, environment);
  }

  public static EnvironmentOperation recordSupportEnded(String environment, ParticipantSpec participant) {
    return new RecordSupportEnded(participant, environment);
  }

  public static EnvironmentOperation recordDeployment(String environment, ParticipantSpec participant) {
    return new RecordDeployment(participant, environment);
  }

  public static EnvironmentOperation recordUndeployment(String environment, ParticipantSpec participant) {
    return new RecordUndeployment(participant, environment);
  }

  EnvironmentOperation(OperationType type, ParticipantSpec participant, String environment) {
    this.type = type;
    this.participant = participant;
    this.environment = environment;
  }

  abstract HttpStatusCode apply(BrokerFeignClient client);

  public final OperationResult execute(BrokerFeignClient client, AtomicBoolean proceed) {
    if (!proceed.get()) {
      return new OperationResult(type, environment, participant, OperationStatus.SKIPPED);
    }

    var httpStatus = apply(client);
    var status = httpStatus.is2xxSuccessful() ? OperationStatus.SUCCESS : OperationStatus.FAILED;
    if (status == OperationStatus.FAILED) {
      proceed.set(false);
    }

    return new OperationResult(type, environment, participant, status);
  }

  @Override
  public String toString() {
    return type.code() + ' ' + participant + " to " + environment;
  }

}