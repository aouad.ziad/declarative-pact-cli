package za.showcase.pact.cli.operations;

import za.showcase.pact.cli.OperationResult;
import za.showcase.pact.cli.feign.BrokerFeignClient;
import za.showcase.pactflow.client.api.model.CreateUpdateEnvironment;
import org.springframework.http.HttpStatusCode;

class CreateEnvironment extends EnvironmentOperation {

  CreateEnvironment(String environment) {
    super(OperationResult.OperationType.CREATE_ENVIRONMENT, null, environment);
  }

  @Override
  HttpStatusCode apply(BrokerFeignClient client) {
    var result = client.createEnvironment(
        new CreateUpdateEnvironment().name(environment)
    );

    return result.getStatusCode();
  }
}
