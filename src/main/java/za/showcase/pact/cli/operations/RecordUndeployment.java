package za.showcase.pact.cli.operations;

import za.showcase.pact.cli.OperationResult;
import za.showcase.pact.cli.feign.BrokerFeignClient;
import za.showcase.pact.cli.specs.ParticipantSpec;
import za.showcase.pactflow.client.api.model.EmbeddedDeployedVersion;
import org.springframework.http.HttpStatusCode;

import java.util.Objects;
import java.util.UUID;

class RecordUndeployment extends EnvironmentOperation {

  RecordUndeployment(ParticipantSpec participant, String environment) {
    super(OperationResult.OperationType.RECORD_UNDEPLOYMENT, participant, environment);
  }

  @Override
  public HttpStatusCode apply(BrokerFeignClient client) {
    var envId = client.getEnvironment(environment).get().getUuid();
    var deployed = findDeployedParticipantUUID(client, participant, envId);
    var response = client.recordUndeployment(deployed);
    return response.getStatusCode();
  }


  private static UUID findDeployedParticipantUUID(BrokerFeignClient client, ParticipantSpec p, UUID envId) {
    return client.getDeployedVersions(envId).getEmbedded().getDeployedVersions().stream()
        .filter(r -> {
          return
              Objects.equals(r.getEmbedded().getPacticipant().getName(), p.name())
                  && Objects.equals(r.getEmbedded().getVersion().getNumber(), p.version());
        })
        .map(EmbeddedDeployedVersion::getUuid)
        .findFirst()
        .orElseThrow(() -> new IllegalStateException("Unable to find the UUID of deployed " + p));
  }
}
