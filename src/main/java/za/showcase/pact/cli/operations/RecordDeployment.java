package za.showcase.pact.cli.operations;

import za.showcase.pact.cli.OperationResult;
import za.showcase.pact.cli.feign.BrokerFeignClient;
import za.showcase.pact.cli.specs.ParticipantSpec;
import org.springframework.http.HttpStatusCode;

class RecordDeployment extends EnvironmentOperation {

  RecordDeployment(ParticipantSpec participant, String environment) {
    super(OperationResult.OperationType.RECORD_DEPLOYMENT, participant, environment);
  }

  @Override
  public HttpStatusCode apply(BrokerFeignClient client) {
    var envId = client.getEnvironment(environment).get().getUuid();
    var response = client.recordDeployment(participant.name(), participant.version(), envId);
    return response.getStatusCode();
  }
}
