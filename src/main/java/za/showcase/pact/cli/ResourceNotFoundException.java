package za.showcase.pact.cli;

import java.nio.file.Path;

public class ResourceNotFoundException extends BaseCliException {

  private final Path path;

  public ResourceNotFoundException(Path path) {
    super(path + " not found");
    this.path = path;
  }

  @Override
  public Problem problem() {
    return Problem.builder()
        .withCode("resource-not-found")
        .with("resource", path.toString())
        .build();
  }
}
