package za.showcase.pact.cli;

public class EnvironmentNotFoundException extends BaseCliException {

  private final String environment;

  public EnvironmentNotFoundException(String environment) {
    super(environment + " not found");
    this.environment = environment;
  }

  @Override
  public Problem problem() {
    return Problem.builder()
        .withCode("environment-not-found")
        .with("environment", environment)
        .build();
  }
}
