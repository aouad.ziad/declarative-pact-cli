package za.showcase.pact.cli;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import za.showcase.pact.cli.specs.EnvironmentSpec;

import java.util.List;

public record ApplySpecResult(
    ApplySpecStatus status,
    @JsonProperty("initial-state") EnvironmentSpec initialState,
    @JsonProperty("final-state") EnvironmentSpec finalState,
    List<OperationResult> operations) {

  enum ApplySpecStatus {
    SUCCESS,
    FAILED,
    PREVIEW;

    @JsonValue
    public String code() {
      return name().toLowerCase();
    }
  }
}